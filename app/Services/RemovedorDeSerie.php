<?php


namespace App\Services;

use App\Episodio;
use App\Serie;
use App\Temporada;
use Illuminate\Support\Facades\DB;

/**
 * Class RemovedorDeSerie
 * @package App\Services
 */
class RemovedorDeSerie
{
    /**
     * @param int $serieId
     * @return string
     */
    public function removerSerie(int $serieId)
    {
        $nomeSerie = "";
        DB::transaction(function () use ($serieId, &$nomeSerie) {
            $serie = Serie::find($serieId);
            $nomeSerie = $serie->nome;
            $this->removerTempordas($serie);
            $serie->delete();
        });

        return $nomeSerie;
    }

    /**
     * @param Serie $serie
     */
    private function removerTempordas(Serie $serie): void
    {
        $serie->temporadas()->each(function (Temporada $temporada) {
            $this->removerEpisodios($temporada);
            $temporada->delete();
        });
    }

    /**
     * @param Temporada $temporada
     */
    private function removerEpisodios(Temporada $temporada): void
    {
        $temporada->episodios()->each(function (Episodio $episodio) {
            $episodio->delete();
        });
    }
}
