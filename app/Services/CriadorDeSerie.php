<?php

namespace App\Services;

use App\Serie;
use App\Temporada;
use Illuminate\Support\Facades\DB;

/**
 * Class CriadorDeSerie
 * @package App\Services
 */
class CriadorDeSerie
{
    /**
     * @param string $nome
     * @param int $qtdTempordas
     * @param int $epPorTemporda
     * @return Serie
     */
    public function criarSerie(string $nome, int $qtdTempordas, int $epPorTemporda): Serie
    {
        DB::beginTransaction();
        $serie = Serie::create(['nome' => $nome]);
        $this->criarTemporadas($serie, $qtdTempordas, $epPorTemporda);
        DB::commit();

        return $serie;
    }

    /**
     * @param Serie $serie
     * @param int $qtdTempordas
     * @param int $epPorTemporda
     */
    private function criarTemporadas(Serie $serie, int $qtdTempordas, int $epPorTemporda): void
    {
        for ($i = 1; $i <= $qtdTempordas; $i++) {
            $temporada = $serie->temporadas()->create(['numero' => $i]);
            $this->criarEpisodios($temporada, $epPorTemporda);
        }
    }

    /**
     * @param Temporada $temporada
     * @param int $epPorTemporda
     */
    public function criarEpisodios(Temporada $temporada, int $epPorTemporda): void
    {
        for ($j = 1; $j <= $epPorTemporda; $j++) {
            $temporada->episodios()->create(['numero' => $j]);
        }
    }
}
