# Primeiros passos

## Passo 1
Utilize o [composer](https://getcomposer.org/) para instalar as dependências.

```bash
composer install
```

## Passo 2
Gere a chave para lavavel com o artisan

```bash
php artisan key:generate
```

## Passo 3
Agora com o artisan importe as migrations

```bash
php artisan migrate
```

## Passo 4
Suba o servido com o artisan

```bash
php artisan serve
```

## Passo 5
Acesse o seu localhost:

```bash
http://localhost:8000/series
```

