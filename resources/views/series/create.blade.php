@extends('layout')

@section('cabecalho')
    Adicionar Série
@endsection

@section('conteudo')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="post" action="/series/criar">
        @csrf
        <div class="row">
            <div class="col col-8">
                <label for="nome">Nome</label>
                <input class="form-control" type="text" name="nome">
            </div>

            <div class="col col-2">
                <label for="qtd_temporadas">Nº tempotadas</label>
                <input class="form-control" type="number" name="qtd_temporadas">
            </div>

            <div class="col col-2">
                <label for="ep_por_temporada">Ep. por temporada</label>
                <input class="form-control" type="number" name="ep_por_temporada">
            </div>

        </div>
        <button class="btn btn-primary mt-2">Adicionar</button>
    </form>
@endsection
